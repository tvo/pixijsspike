
// for hexigons
class HexagonPoint {

	constructor(x, y, z, control) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.control = (control == true);
	}

	toPixel() {
		return new PixelPoint(this.x * 0.75, Math.sqrt(3)/2 * (this.x/2 + this.y));
	};
}

// x y point class
class PixelPoint {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

	shift(x, y) {
		return new PixelPoint(this.x + x, this.y + y);
	}

	multiply(s) {
		return new PixelPoint(this.x * s, this.y * s);
	}
}
