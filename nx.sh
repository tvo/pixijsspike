#!/bin/bash

docker stop pixijs
docker rm pixijs
docker run --name pixijs -p 8000:80 -v "$PWD:/usr/share/nginx/html:ro" -d nginx
