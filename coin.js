class Coin
{
	container = null;

	constructor (pixel, boardHexagons, label) {
		this.board = boardHexagons;
		this.pixel = pixel;

		this.sprite = new PIXI.Graphics();

		this.text = new PIXI.Text(label, { 
			fill: 0xFFFFFF,
			fontSize: 16,
			fontFamily: 'Arial',
		});
		this.text.anchor.set(0.5);

		this.updatePosition(pixel);
		this.draw();
		this.setInteractive();
	}

	setInteractive() {
		this.sprite.interactive = true;
		this.sprite.buttonMode = true;
		this.sprite.dragging = false;

		this.sprite
			.on('mousedown', e => this.onDragStart(e))
			.on('touchstart', e => this.onDragStart(e))
			.on('mouseup', () => this.onDragEnd())
			.on('mouseupoutside', () => this.onDragEnd())
			.on('touchend', () => this.onDragEnd())
			.on('touchendoutside', () => this.onDragEnd())
			.on('mousemove', () => this.onDragMove())
			.on('touchmove', () => this.onDragMove());
	}

	draw() {
		let c = {
			lineColor: 0xAA0000,
			fillColor: 0x111111,
			radius: 30,
		};

		this.sprite.clear();

		this.sprite.beginFill(c.fillColor);
		this.sprite.lineStyle({
			width: 3,
			alignment: 0.5,
			color: c.lineColor,
		});
		this.sprite.drawCircle(0, 0, c.radius);
		this.sprite.endFill();
	}

	updatePosition(pixel) {
		this.pixel = pixel;
		this.sprite.x = pixel.x;
		this.sprite.y = pixel.y;
		this.text.x = pixel.x;
		this.text.y = pixel.y;
	}

	onDragStart(event) {
		this.sprite.data = event.data;
		this.sprite.alpha = 0.5;
		this.dragging = true;

		if (this.container != null) {
			this.container.unstack(this);
			this.container = null;
		}
	}

	onDragEnd() {
		if (!this.dragging)
			return;
		this.dragging = false;
		this.sprite.alpha = 1;
		this.sprite.data = null;

		// this is the snap behavior for the grid system.
		var hex = this.board.find(e => e.isCloseTo(this.pixel));
		if(hex == null || hex == undefined)
			return;

		this.container = hex;
		hex.stack(this);
	}

	onDragMove() {
		if(!this.dragging)
			return;

		var newPosition = this.sprite.data.getLocalPosition(this.sprite.parent);
		this.updatePosition(newPosition);
	}

	addToApp(app) {
		app.stage.addChild(this.sprite);
		app.stage.addChild(this.text);
	}
}
