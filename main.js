PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

//Create a Pixi Application
let app = new PIXI.Application({
	backgroundColor: 0x222222, 
	antialias: false,
	autoDensity: true,
});
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
//app.renderer.autoResize = true;
//app.renderer.resize(window.innerWidth, window.innerHeight);
app.renderer.resize(window.innerWidth, 900);
//app.renderer.resize(900, 900);

//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);

let gameService = new GameService(10, 2, app);
//let gameService = new GameService(10, 2, app);
gameService.loadGame();

