function makeRequest(url, responseCb, headers) {
	let xhttp = new XMLHttpRequest();
	xhttp.addEventListener("load", function() {
		let json = JSON.parse(xhttp.responseText);
		responseCb(json);
	});
	xhttp.open("GET", url, true);
	if(headers) {
		for (let [key, value] of Object.entries(headers)) {
			xhttp.setRequestHeader(key, value);
		}
	}

	xhttp.send();
}

class GameService {
	hexagons = [];
	holders = [];

	constructor(gameId, playerId, app) {
		this.gameId = gameId;
		this.playerId = playerId;
		this.app = app;

		this.centerX = app.screen.width / 2;
		this.centerY = app.screen.height / 2;

		this.addHolders();
	}

	selectableForCoinDrop() {
		let result = [];
		this.hexagons.forEach(h => result.push(h));
		this.holders.forEach(h => result.push(h));
		return result;
	}

	addHolders() {
		let b = 4;
		this.holders = [
			new CoinHolder(new PixelPoint(this.centerX + b, 650), 'bag'),
			new CoinHolder(new PixelPoint(this.centerX + b, 750), 'hand'),
			new CoinHolder(new PixelPoint(this.centerX - 400 - b, 650), 'discard'),
			new CoinHolder(new PixelPoint(this.centerX - 400 - b, 750), 'supply'),
		];
		this.holders.forEach(e => e.addToApp(this.app));
	}
	
	addHexagons() {
		this.hexagons = this.hexagons.map(e => e.multiply(100).shift(this.centerX, 320));
		this.hexagons.forEach(e => e.addToApp(this.app));
	}

	addCoins() {
		let all = this.selectableForCoinDrop();
		let playerCards = this.game.coinsOnBoard.coinsInBag
			.filter(card => card.player.playerId == this.playerId);
		console.log(playerCards);

		let defaultSpot = new PixelPoint(300, 300);
		let coins = playerCards.map(c => new Coin(defaultSpot, all, c.coin));
		coins.forEach(e => e.addToApp(this.app));
		let bag = this.holders[0];
		coins.forEach(c => bag.stack(c));
	}

	isFourPlayer(gameDto) {
		return true;
	}

	loadGame() {
		let url = "http://tvonsegg.com/api/game/" + this.gameId + '/state';
		makeRequest(url, json => {
			this.game = json.content;
			this.loadBoardLayout();
		}, { playerId: this.playerId });
	}
	loadBoardLayout() {
		//let players = this.isFourPlayer() ? '4' : '2';
		let url = "http://tvonsegg.com/api/game/board/" + this.game.players.length;
		makeRequest(url, json => {
			this.hexagons = json
				.map(e => new HexagonPoint(e.x, e.y, e.z, e.control))
				.map(e => new Hexagon(e));
			this.addHexagons();
			this.addCoins();
		});
	}
}

/*
let centerX = app.screen.width / 2;
let centerY = app.screen.height / 2;

let hexagons = Hexagon.FourPlayerBoard
	.map(e => {
		return e.multiply(100).shift(centerX, 320)
	});

let b = 4;
let holders = [
	new CoinHolder(new PixelPoint(centerX + b, 650), 'bag'), // bag
	new CoinHolder(new PixelPoint(centerX + b, 750), 'hand'), // hand
	new CoinHolder(new PixelPoint(centerX - 400 - b, 650)),
	new CoinHolder(new PixelPoint(centerX - 400 - b, 750)),
];

holders.forEach(h => hexagons.push(h)); // holders => hexagons

var coins = [
	new Coin(new PixelPoint(500, 500), hexagons, 'Archer'),
	new Coin(new PixelPoint(300, 200), hexagons, 'Scout'),
];

hexagons.forEach(x => x.addToApp(app));
coins.forEach(x => x.addToApp(app));

*/
