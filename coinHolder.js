class CoinHolder
{
	coinStack = [];

	constructor (pixel, label) {
		this.pixel = pixel;

		let graphic = new PIXI.Graphics();
		this.graphic = graphic;

		this.text = new PIXI.Text(label, {
			fill: 0xFFFFFF,
			fontSize: 16,
			fontFamily: 'Arial',
		});
		
		this.height = 92;
		this.width = 400;

		graphic.beginFill(0x333333);
		graphic.drawRect(0, 0, this.width, this.height);
		graphic.endFill();

		this.updatePosition(pixel);
	}

	stack(coin) {
		coin.container = this;
		this.coinStack.push(coin);
		this.repositionStack();
	}

	unstack(coin) {
		this.coinStack = this.coinStack.filter(x => x != coin);
		this.repositionStack();
	}

	repositionStack() {
		for(let i = 0; i < this.coinStack.length; ++i) {
			let coin = this.coinStack[i];

			let x = coin.sprite.width / 2 + 100 * i;
			let y = coin.sprite.height / 2;

			let padding = 5;
			x += padding; y += padding;
			let newP = this.pixel.shift(x, y);
			coin.updatePosition(newP);
		}
	}

	isCloseTo(pixel) {
		let p = this.pixel;

		if(pixel.x < p.x || pixel.y < p.y)
			return false;
		p = p.shift(this.width, this.height);
		if(pixel.x > p.x || pixel.y > p.y)
			return false;
		return true;
	}

	updatePosition(pixel) {
		this.pixel = pixel;
		this.graphic.x = pixel.x;
		this.graphic.y = pixel.y;
		this.text.x = pixel.x;
		this.text.y = pixel.y;
	}

	addToApp(app) {
		app.stage.addChild(this.graphic);
		if(this.text)
			app.stage.addChild(this.text);
	}
}
