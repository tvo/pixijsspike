class Hexagon
{
	constructor (h) {
		this.controlTeam = undefined;
		this.pixel = h.toPixel();
		this.hex = h;

		this.graphic = new PIXI.Graphics();
		this.draw();

		//if(h.control) {
			//this.text = new PIXI.Text('c', { 
				//fill: 0xFFFFFF,
				//fontSize: 16,
				//fontFamily: 'Arial',
			//});
			//this.text.anchor.set(0.5);
		//}
	}
	
	lineColor() {
		if(this.controlTeam === undefined)
			return 0xCCCCCC;
		if(this.controlTeam === true)
			return 0xCC0000;
		return 0x0000CC;
	}

	draw() {
		this.graphic.clear();
		let hexagonRadius = 46;
		let hexagonHeight = hexagonRadius * Math.sqrt(3);
		//this.graphic.beginFill(0x444444);
		
		let color = this.lineColor();
		if(this.hex.control)
			color = 0x00AA00;
		this.graphic.lineStyle({
			width: 3,
			alignment: 0.5,
			color: color,
		});
		this.graphic.drawPolygon([
		  -hexagonRadius, 0,
		  -hexagonRadius/2, hexagonHeight/2,
		  hexagonRadius/2, hexagonHeight/2,
		  hexagonRadius, 0,
		  hexagonRadius/2, -hexagonHeight/2,
		  -hexagonRadius/2, -hexagonHeight/2,
		]);
		this.graphic.endFill();
	}

	stack(coin) {
		coin.updatePosition(this.pixel);
	}
	unstack(coin) { }

	isCloseTo(pixelPoint) {
		var a = this.pixel.x - pixelPoint.x;
		var b = this.pixel.y - pixelPoint.y;
		var c = Math.sqrt( a*a + b*b );
		return c < 50;
	}

	updatePosition() {
		this.graphic.x = this.pixel.x;
		this.graphic.y = this.pixel.y;
		if(this.text) {
			this.text.x = this.pixel.x;
			this.text.y = this.pixel.y;
		}
	}

	multiply(s) {
		this.pixel = this.pixel.multiply(s);
		this.updatePosition();
		return this;
	}

	shift(x, y) {
		this.pixel = this.pixel.shift(x, y);
		this.updatePosition();
		return this;
	}

	addToApp(app) {
		app.stage.addChild(this.graphic);
		if(this.text) {
			app.stage.addChild(this.text);
		}
	}
}
